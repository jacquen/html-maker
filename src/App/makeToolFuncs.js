import {RichUtils} from "draft-js";

function makeToolFuncs(app) {
    "use strict";

    const obj = {
        boldClick(e) {
            e.preventDefault();
            app.onChange(RichUtils.toggleInlineStyle(app.state.editorState, "BOLD"));
        },
        italicClick(e) {
            e.preventDefault();
            app.onChange(RichUtils.toggleInlineStyle(app.state.editorState, "ITALIC"));
        },
        underlineClick(e) {
            e.preventDefault();
            app.onChange(RichUtils.toggleInlineStyle(app.state.editorState, "UNDERLINE"));
        },
        fontSizeClick(fontSize) {
            let prevSize = app.getStyle("fontSize");
            if (prevSize) {
                app.onChange(
                    RichUtils.toggleInlineStyle(app.state.editorState, prevSize),
                    function () {
                        app.onChange(RichUtils.toggleInlineStyle(app.state.editorState, fontSize));
                    }
                );
            } else {
                app.onChange(RichUtils.toggleInlineStyle(app.state.editorState, fontSize));
            }
        }
    };

    return obj;
}

module.exports = makeToolFuncs;
