import React from "react";
require("./toolbar.css");
const style = require("./toolbar.css.js");
const FontDropdown = require("./FontDropdown/FontDropdown.jsx");

function Toolbar(props) {
    "use strict";

    const obj = {
        __proto__: React.Component.prototype
    };

    const {boldClick, italicClick, underlineClick} = props.toolFuncs;

    obj.render = function () {
        return (
            <div>
                <div style={style.toolbar} onClick={props.focus}>
                    <div className="tool" style={style.bold} onMouseDown={boldClick}>
                        B
                    </div>
                    <div className="tool" style={style.italic} onMouseDown={italicClick}>
                        I
                    </div>
                    <div className="tool" style={style.underline} onMouseDown={underlineClick}>
                        U
                    </div>
                    <FontDropdown getStyle={props.getStyle}
                            fontSizeClick={props.toolFuncs.fontSizeClick} />
                </div>
                <div style={style.toolbarBottom}>{`what ${style.toolbarBottom.height}`}</div>
            </div>
        );
    };

    return obj;
}

module.exports = Toolbar;
