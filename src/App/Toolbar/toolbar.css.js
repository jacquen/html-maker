const CSS = require("../css.js");

const height = 28;

const style = {
    toolbar: {
        border: `solid 1px ${CSS.borderGray}`,
        textAlign: "center",
        position: "fixed",
        width: "100%",
        zIndex: "400",
        height: height + "px",
        background: CSS.backgroundGray,
        paddingTop: "2px"
    },
    bold: {
        fontWeight: "bold"
    },
    italic: {
        fontStyle: "italic"
    },
    underline: {
        textDecoration: "underline"
    },
    toolbarBottom: {
        height: height + 10 + "px"
    }
};

style.toolbar = Object.assign(style.toolbar, CSS.noSelect);

module.exports = style;
