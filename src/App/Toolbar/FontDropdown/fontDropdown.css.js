const CSS = require("../../css.js");

const style = {
    fontSizes: {
        zIndex: "1000",
        display: "none",
        position: "absolute",
        top: "30px",
        textAlign: "center",
        background: "white",
        border: `solid 1px ${CSS.borderGray}`,
        width: "60px",
        padding: "5px 0 5px"
    },
    fontSize: {
        display: "inline-block",
        textAlign: "center",
        width: "100%",
        boxSizing: "border-box",
        padding: "5px"
    }
};

module.exports = style;
