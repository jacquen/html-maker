import React from "react";
import update from "react-addons-update";
const style = require("./fontDropdown.css.js");
require("./fontDropdown.css");
const fontSizes = require("../../fontSizes.js");

function FontDropdown(props) {
    "use strict";

    const obj = {
        __proto__: React.Component.prototype,
        state: {style}
    };

    function hideDropdown() {
        obj.setState({
            style: update(obj.state.style, {
                fontSizes: {display: {$set: "none"}}
            })
        });
    }

    function handleOutsideClick(event) {
        if (obj.wrapperRef && !obj.wrapperRef.contains(event.target)) {
            hideDropdown();
        }
    }

    obj.componentDidMount = function () {
        document.addEventListener("mousedown", handleOutsideClick);
    };

    obj.componentWillUnmount = function () {
        document.removeEventListener("mousedown", handleOutsideClick);
    };

    function currentFontSize() {
        let value = props.getStyle("fontSize");
        if (value) {
            return value.replace(/FONT_/, "") + "px";
        } else {
            return "16px";
        }
    }

    function fontSizeMenuClick() {
        let newDisplay = "none";
        if (obj.state.style.fontSizes.display === "none") {
            newDisplay = "block";
        }
        obj.setState({
            style: update(obj.state.style, {
                fontSizes: {display: {$set: newDisplay}}
            })
        });
    }

    function sendFontSize(fontSize, e) {
        e.preventDefault();
        props.fontSizeClick(fontSize);
        hideDropdown();
    }

    const fontSizeDivs = fontSizes.map(function (fontSizeValue) {
        return (
            <div
                key={fontSizeValue}
                style={style.fontSize}
                onMouseDown={sendFontSize.bind(null, fontSizeValue)}
            >
                {fontSizeValue.replace(/FONT_/, "") + "px"}
            </div>
        )
    });

    function objRef(node) {
        obj.wrapperRef = node;
    }

    obj.render = function () {
        return (
            <div className="font-dropdown" ref={objRef}>
                <span id="font-size-menu" onMouseDown={fontSizeMenuClick}>
                    {currentFontSize()}
                </span>
                <div style={obj.state.style.fontSizes}>{fontSizeDivs}</div>
            </div>
        );
    };

    return obj;
}

module.exports = FontDropdown;
