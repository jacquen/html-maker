const CSS = require("../css.js");

const style = {
    menu: {
        background: "white",
        border: `solid 1px ${CSS.borderGray}`,
        height: "30px",
        cursor: "default",
        position: "fixed",
        width: "100%",
        zIndex: "500"
    },
    fileWrapper: {
        margin: "3px 0 0 40px"
    },
    file: {
        display: "inline-block",
        padding: "4px 10px"
    },
    fileDropdown: {
        display: "none",
        background: "white",
        width: "200px",
        border: `solid 1px ${CSS.borderGray}`,
        padding: "5px 0",
        marginTop: "-1px"
    },
    fileItem: {
        padding: "2px 40px"
    },
    menuBottom: {
        height: "30px"
    }
};

style.menu = Object.assign(style.menu, CSS.noSelect);

module.exports = style;
