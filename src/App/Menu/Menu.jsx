import React from "react";
import update from "react-addons-update";
const menuStyle = require("./menu.css.js");
const CSS = require("../css.js");
require("./menu.css");

function Menu(props) {
    "use strict";

    const obj = {
        __proto__: React.Component.prototype,
        state: {menuStyle}
    };

    function offFileClick(event) {
        if (obj.fileWrapperNode && !obj.fileWrapperNode.contains(event.target)) {
            fileClick(true);
        }
    }

    obj.componentDidMount = function () {
        document.addEventListener("mousedown", offFileClick);
    };

    obj.componentWillUnmount = function () {
        document.removeEventListener("mousedown", offFileClick);
    }

    function updateFileWrapper(newStyle) {
        obj.setState({
            menuStyle: update(obj.state.menuStyle, {
                file: {
                    background: {$set: newStyle.background},
                    border: {$set: newStyle.border},
                    borderBottom: {$set: newStyle.borderBottom}
                },
                fileDropdown: {display: {$set: newStyle.display}}
            })
        })
    }

    function fileClick(offClick) {
        let newStyle = {
            display: "block",
            background: "white",
            border: `solid 1px ${CSS.borderGray}`,
            borderBottom: "solid 1px white"
        };
        if ((obj.state.menuStyle.fileDropdown.display === "block") || offClick) {
            newStyle = {
                display: "none",
                background: "",
                border: "",
                borderBottom: ""
            };
        }
        updateFileWrapper(newStyle);
    }

    function fileWrapperNode(node) {
        obj.fileWrapperNode = node;
    }

    obj.render = function () {
        return (
            <div>
                <div style={menuStyle.menu}>
                    <div style={menuStyle.fileWrapper} ref={fileWrapperNode}>
                        <div className={"file"} style={obj.state.menuStyle.file}
                                onMouseDown={fileClick.bind(null, false)}>
                            File
                        </div>
                        <div style={obj.state.menuStyle.fileDropdown}>
                            <div className={"file-item"} style={menuStyle.fileItem}
                                    onClick={props.showHTML}>
                                Show as HTML
                            </div>
                        </div>
                    </div>
                </div>
                <div style={menuStyle.menuBottom}></div>
            </div>
        );
    };

    return obj;
}

module.exports = Menu;
