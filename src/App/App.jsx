import React from "react";
import {Editor, EditorState, RichUtils} from "draft-js";
import {stateToHTML} from "draft-js-export-html";
require("./app.css");
const appStyle = require("./app.css.js");
const fontSizes = require("./fontSizes.js");
const styleMap = require("./styleMap.js");
const exportOptions = require("./exportOptions.js");
const Menu = require("./Menu/Menu.jsx");
const Toolbar = require("./Toolbar/Toolbar.jsx");
const Modal = require("./Modal/Modal.jsx");
const makeToolFuncs = require("./makeToolFuncs.js");

function App(props) {
    "use strict";

    const obj = {
        __proto__: React.Component.prototype,
        state: {
            editorState: EditorState.createEmpty(),
            modalClassName: "inactive"
        }
    };

    const toolFuncs = makeToolFuncs(obj);

    obj.onChange = function (editorState, callback) {
        obj.setState({editorState}, callback);
    };

    function handleKeyCommand(command) {
        const newState = RichUtils.handleKeyCommand(obj.state.editorState, command);
        if (newState) {
            obj.onChange(newState);
            return "handled";
        }
        return "not-handled";
    }

    function hasStyle(style) {
        return obj.state.editorState.getCurrentInlineStyle().has(style);
    }

    obj.getStyle = function (style) {
        if (style === "fontSize") {
            let i;
            for (i = 0; i < fontSizes.length; i += 1) {
                if (hasStyle(fontSizes[i])) {
                    return fontSizes[i];
                }
            }
        }
    };

    function showHTML() {
        const currentContent = obj.state.editorState.getCurrentContent()
        const html = stateToHTML(currentContent, exportOptions);
        obj.setState({
            modalClassName: "active",
            modalHTML: html
        });
    }

    function closeModal() {
        obj.setState({modalClassName: "inactive"});
    }

    function focus() {
        obj.refs.editor.focus();
    }

    function getModalClassName() {
        return obj.state.modalClassName;
    }

    function getModalHTML() {
        return obj.state.modalHTML;
    }

    obj.render = function () {
        return (
            <div style={appStyle}>
                <Menu showHTML={showHTML}/>
                <Toolbar focus={focus} toolFuncs={toolFuncs} getStyle={obj.getStyle} />
                <Modal getModalClassName={getModalClassName}
                        closeModal={closeModal} getModalHTML={getModalHTML}/>
                <Editor
                    customStyleMap={styleMap}
                    editorState={obj.state.editorState}
                    handleKeyCommand={handleKeyCommand}
                    onChange={obj.onChange}
                    ref="editor"
                />
            </div>
        );
    };

    return obj;
}

module.exports = App;
