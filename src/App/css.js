module.exports = {
    backgroundGray: "#eee",
    borderGray: "#d9d9d9",
    noSelect: {
        WebkitTouchCallout: "none",
        WebkitUserSelect: "none",
        khtmlUserSelect: "none",
        MozUserSelect: "none",
        msUserSelect: "none",
        userSelect: "none"
    }
};
