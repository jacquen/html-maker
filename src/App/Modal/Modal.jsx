import React from "react";
const modalStyle = require("./modal.css.js");

function Modal(props) {
    "use strict";

    const obj = {
        __proto__: React.Component.prototype
    };

    function handleOutsideClick(event) {
        if (obj.node && !obj.node.contains(event.target)) {
            props.closeModal();
        }
    }

    obj.componentDidMount = function () {
        document.addEventListener("mousedown", handleOutsideClick);
    };

    obj.componentWillUnmount = function () {
        document.removeEventListener("mousedown", handleOutsideClick);
    };

    function objRef(node) {
        obj.node = node;
    }

    obj.render = function () {
        return (
            <div style={modalStyle} className={props.getModalClassName()}
                    ref={objRef}>
                {props.getModalHTML()}
            </div>
        );
    };

    return obj;
}

module.exports = Modal;
