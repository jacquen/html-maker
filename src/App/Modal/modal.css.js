module.exports = {
    position: "fixed",
    zIndex: "1000",
    background: "white",
    top: "0",
    bottom: "0",
    left: "0",
    right: "0",
    margin: "auto",
    width: "80%",
    height: "80%",
    border: "solid 1px black",
    borderRadius: "4px",
    padding: "30px",
    boxSizing: "border-box"
};
