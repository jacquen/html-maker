const styleMap = require("./styleMap.js");

let options = {};

Object.keys(styleMap).forEach(function (key) {
    "use strict";

    options[key] = {style: styleMap[key]};
});

module.exports = {
    inlineStyles: options
};
