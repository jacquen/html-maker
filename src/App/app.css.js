const CSS = require("./css.js");

module.exports = {
    background: CSS.backgroundGray,
    width: "100%",
    height: "100%"
};
