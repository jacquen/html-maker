module.exports = {
    "FONT_12": {fontSize: "12px"},
    "FONT_14": {fontSize: "14px"},
    "FONT_16": {fontSize: "16px"},
    "FONT_18": {fontSize: "18px"},
    "FONT_20": {fontSize: "20px"},
    "FONT_22": {fontSize: "22px"},
    "FONT_26": {fontSize: "26px"},
    "FONT_30": {fontSize: "30px"},
    "FONT_36": {fontSize: "36px"},
    "FONT_42": {fontSize: "42px"},
    "FONT_55": {fontSize: "55px"},
    "FONT_70": {fontSize: "70px"},
    "FONT_90": {fontSize: "90px"},
    "FONT_130": {fontSize: "130px"}
};
