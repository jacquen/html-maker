import React from "react";
import ReactDOM from "react-dom";
const App = require("./App/App.jsx");

ReactDOM.render(
    <App />,
    document.getElementById("root")
);
