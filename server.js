const express = require("express");
const webpack = require("webpack");
const path = require("path");

const webpackConfig = require("./webpack.config");
const compiler = webpack(webpackConfig);

const app = express();

app.use(require("webpack-dev-middleware")(compiler));

app.get("/", function (req, res) {
    res.sendFile(path.join(__dirname, "index.html"));
});

const port = process.env.PORT || 3000;

app.listen(port, function () {
    console.log("Server listening on port " + port);
});
